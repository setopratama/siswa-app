@extends('templates.default')

@section('content')
<div class="card">
    <div class="table-responsive">
      <table class="table table-vcenter card-table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Create</th>
            <th class="w-1"></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($users as $user)
          <tr>
            <td>{{ $user->name }}</td>
            <td class="text-secondary">{{ $user->email }}</td>
            <td class="text-secondary">{{ $user->created_at }}</td>
            <td>
              <a href="#">Edit</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection